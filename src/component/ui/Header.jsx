import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

function Header() {

return (
  <Navbar collapseOnSelect expand="lg"  bg="primary" variant="dark">
    <Container>
      <Navbar.Brand href="#home">Covid Serbia Tracker</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link href="#features">Home</Nav.Link>
          <Nav.Link href="https://data.gov.rs/sr/datasets/covid-19-dnevni-izveshtaj-o-epidemioloshkoj-situatsiji-u-republitsi-srbiji/">Data source</Nav.Link>
          
        </Nav>
       
      </Navbar.Collapse>
    </Container>
  </Navbar>
);
}
  

export default Header;
