import React, { useContext, useEffect, useState } from "react";
import Calendar from "react-calendar";

import AppContext from "../../context/context";
const CovidCalendar = () => {

  const [date, setDate] = useState(new Date());
  const {data, setData}= useContext(AppContext);
  const onChange = date => {
    setDate(date);
    
  };
useEffect(()=>{console.log(date)},[date])
  useEffect(() => {

    fetch(`https://covid-web.calmsmoke-3e5c2162.canadacentral.azurecontainerapps.io/?date=${date.toISOString().split("T")[0]}`)
      .then((response) => response.json())
      .then((i) => { setData(i[0]);
        
      })
  }, [date])
  return (
  
      <div className='calendar-container'>
        <Calendar onChange={setDate} value={date} />
      </div>
  
  );
};
export default CovidCalendar;