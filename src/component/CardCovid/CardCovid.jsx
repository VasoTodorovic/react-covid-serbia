import { useContext, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import AppContext from '../../context/context';
import Pie from './Pie';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Load from '../ui/Load';
const CardCovid = (props) => {
    const { data, setData } = useContext(AppContext);
   
    if (true===true) { return (<Load/>) }
    else if (data === undefined) {
        return (<div> Sorry there is no information for this date,
            choose a date between <b>{props.date[0]}</b> and <b>{props.date[1]}</b></div>)
    }
    return (
        <Card id="cardcovid" style={{ width: '24rem' }}>

            <Card.Title style={{"margin-left":"2px"}}>Na datum {data.date}</Card.Title>

            <div className='list-wrap'>
                <ListGroup className="list-group-flush"> 
                <b style={{"margin-left":"6px"}}> Na današnji dan</b>
                   <ListGroup.Item>broj hospitalozovanih: {data.broj_HOSPITALIZOVANIH_LICA_ZA_DATI_DATUM
                }</ListGroup.Item>
                   
                    <ListGroup.Item>Covid pozitivni: {data.broj_POZITIVNIH_LICA_ZA_DATI_DATUM
                    }</ListGroup.Item>
                    <ListGroup.Item>Preminuli: {data.broj_PREMINULIH_LICA_ZA_DATI_DATUM}</ListGroup.Item>
                    <ListGroup.Item>Testirani: {data.broj_TESTIRANIH_LICA_ZA_DATI_DATUM
                    }</ListGroup.Item>
                </ListGroup>
                <ListGroup className="list-group-flush">
                <b> Od početka COVID-a</b>
                    <ListGroup.Item>Covid pozitivni: {data.ukupan_BROJ_POZITIVNIH_LICA_OD_POČETKA_PANDEMIJE
                    }</ListGroup.Item>
                    <ListGroup.Item>Preminuli: {data.ukupan_BROJ_PREMINULIH_LICA_OD_POČETKA_PANDEMIJE}
                    </ListGroup.Item>
                    <ListGroup.Item>Testirani: {data.ukupan_BROJ_TESTIRANIH_LICA_OD_POČETKA_PANDEMIJE
                    }</ListGroup.Item>
                </ListGroup>
            </div>
            <Card.Body>
                <div className="procent">
                   <Container>
                   <Row  className="square border-top border-start border-end border-primary  rounded"><Col xs={4}><Pie percentage={data.procenat_ZARAŽENIH_LICA_OD_POČETKA_PANDEMIJE_U_ODNOSU_NA_UKUPAN_BROJ_TESTIRANIH_LICA
                    } colour="blue"></Pie></Col> <Col xs={8}>procenat zarazenih lica od pocetka pandemije u odnosu na ukupan broj testiranih lica</Col></Row>
                    <Row  className="square border-top border-bottom border-start border-end border-primary rounded"><Col xs={4}>      <Pie percentage={data.procenat_ZARAŽENIH_LICA_U_ODNOSU_NA_BROJ_TESTIRANIH_LICA_ZA_DATI_DATUM
                    } colour="blue" ></Pie></Col> <Col xs={8}><p>procenat zarazenih lica u odnosu na broj testiranh lica za dati datum</p></Col></Row>
                    <Row  className="square border-bottom border-start border-end border-primary rounded  "><Col xs={4}><Pie percentage={data.procenat_HOSPITALIZOVANIH_LICA_U_ODNOSU_NA_UKUPAN_BROJ_ZARAŽENIH_ZA_DATI_DATUM
                    } colour="blue" ></Pie></Col><Col xs={8}><p >procenat hospitalozovanih lica u odnosu na ukupan broj zarazenih za taj dan</p></Col></Row>
                    
                    </Container>
              
                </div>
            </Card.Body>
        </Card>)

}
export default CardCovid;