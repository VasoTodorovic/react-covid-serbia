
import './style/main.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CovidCalendar from './component/Calendar/CovidCalendar';
import CardCovid from './component/CardCovid/CardCovid';
import { AppProvider } from './context/context';
import { useEffect, useState } from "react";
import Header from './component/ui/Header';
function App() {
  const [data, setData] = useState(null);
  const [date, setDate] = useState([]);
  useEffect(() => console.log(data), [data])
  useEffect(() => {

    fetch(`https://covid-web.calmsmoke-3e5c2162.canadacentral.azurecontainerapps.io/minmax`)
      .then((response) => response.json())
      .then((i) => {
        setDate(i)
      })
  }, [])
  return (
    <AppProvider value={{ data, setData }}>
     
      <div className="App">
        
      
        
      <Header></Header>
        <Container>
        <Row className="justify-content-md-center" ><p>Trenutno raspolažamo sa podacima od <strong>{date[0]}</strong> do<strong>{date[1]}</strong> . Izaberite datum u tom periodu
        </p></Row>
        <Row>
          <Col xs={12} md={6}>
          <CovidCalendar />
          </Col>
          <Col xs={12} md={6}>
          <CardCovid date={date} />
          </Col>
        </Row>
      </Container>
        

        
      </div>
    </AppProvider>
  );
}

export default App;
